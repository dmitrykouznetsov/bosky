"""A plot is generated from the input function and optional data.

The most important aspect for speeding up model based data analysis is to dynamically
preset all sliders and other widgets. Adding data to the plot causes most of the
changes to the layout of the widgets. A special DataPlot class inherits the basis
structure of a plot but adds additional callbacks for fitting.
"""
import numpy as np
from lmfit import Parameters, minimize

from bokeh.models import HoverTool, Whisker, ColumnDataSource, Slider, BoxAnnotation, BoxSelectTool
from bokeh.models.widgets import Button, Dropdown, Div
from bokeh.layouts import column, row, widgetbox
from bokeh.plotting import figure

from collections import Counter
from inspect import signature
from .fit import residuals, format_results
from .theme import settings


def optimal_legend_location(source):
    """Decide in which corner the legend should be displayed.

    For each datapoint a unit vector is assigned that denotes to which quadrant
    with respect to the center of the plot the point belongs. The quadrant with
    the least number of points indicates the position of the legend.

    .. todo::
       Account for complex plots

    Args:
        source (ColumnDataSource): Source of the dataset if there are datapoints,
            otherwise the source of the model should be used.

    Returns:
        str: top_left, top_right, bottom_left or bottom_right

    """
    sd = source.data
    center = tuple((min(sd[d]) + 0.5 * np.abs(max(sd[d]) - min(sd[d])) for d in ["x", "y"]))
    coords_as_str = {
        (1, 1): "top_right",
        (-1, 1): "top_left",
        (1, -1): "bottom_right",
        (-1, -1): "bottom_left",
    }
    quadrant_density = Counter({q: 0 for q in coords_as_str.keys()})

    for point in zip(sd["x"], sd["y"]):
        # convert to coord tuple $ zipWith > points center
        unit_quadrant_coord = tuple(map(lambda p, c: 1 if p > c else -1, point, center))
        quadrant_density[unit_quadrant_coord] += 1

    # coords label of least populated [-1] quadrant
    lcomm = quadrant_density.most_common()[-1][0]
    return coords_as_str[lcomm]


class Plot:
    """Setup a Plot object by dynamically creating appropriate widgets specified from the input.

    Args:
        f (function): A function with x as input and named variables in a dict `f(x, p)`.
        params (dict): Labelled parameters initialized by a single number or a range.
        lim (tuple): Plot limits

    """

    def __init__(self, f, params, lim, **additional_figure_settings):
        """Build plot with optional sliders."""
        self.f = f
        self.X = np.linspace(lim[0], lim[1], 80)

        self.state = {}
        self.sliders = []
        self.plots = []

        # Parameters used for fitting, required for lmfit
        self.parameters = Parameters()
        self.state_from_input(params)

        # Take absolute value of data on log scale
        self.logx = True if additional_figure_settings.get("x_axis_type") == "log" else False
        self.logy = True if additional_figure_settings.get("y_axis_type") == "log" else False

        calc = self.f(self.X, self.state) if self.interactive else self.f(self.X)
        self.source_model = ColumnDataSource(self.generate_cds_data(self.X, calc))

        self.x_key = "x" if not self.logx else "x_log"
        self.y_key = "y" if not self.logy else "y_log"

        if self.complex:
            # TODO: filter out and replace y_axis_label with Real(y_acis_label)
            self.y_real_key = "y_real" if not self.logy else "y_real_log"
            self.y_imag_key = "y_imag" if not self.logy else "y_imag_log"

            # Two plots must be stacked
            plot_dimensions = dict(width=600, height=300)

            self.initialize_plot(self.x_key, self.y_real_key, name=self.y_real_key, **plot_dimensions, **additional_figure_settings)
            self.initialize_plot(self.x_key, self.y_imag_key, name=self.y_imag_key, **plot_dimensions, **additional_figure_settings)
        else:
            self.initialize_plot(self.x_key, self.y_key, name=self.y_key, **additional_figure_settings)

        # Initialize widgets
        self.div = Div(text="")
        self.buttons = []
        button_export = Dropdown(
            label="Export as ...",
            menu=[
                ("PGF Plot", "item_1"),
                ("PNG", "item_2")
            ]
        )
        self.buttons.append(button_export)

    def initialize_plot(self, x_key, y_key, name="", **additional_figure_settings):
        """Setup figure and draw line of the model."""
        plot = figure(**settings["figure"], **additional_figure_settings)
        plot.line(
            x=x_key, y=y_key, source=self.source_model, name=name, **settings["line"])

        self.plots.append(plot)

    @property
    def interactive(self):
        """Decide if the plot should be interactive based on number of function arguments.

        Returns:
            bool: True if function has form f(x, p) or False if the form is f(x)

        Raises:
            AssertionError: The function should not have more than 2 arguments.
                For example f(x, p, something_else) is not allowed.

        """
        args = signature(self.f).parameters
        nargs = len(args)

        assert nargs <= 2, f"Function should have at most two input parameters: \
                f(x, p) where p is the parameter dict, got {str(args)}"

        return True if nargs == 2 else False

    @property
    def complex(self):
        """Check if the function output is complex.

        The comdition is verified by checking if the class name of the first element
        of the evaluated function has "complex" in its name (e.g. "complex128").
        Models that have a complex output will be split into two plots; real and
        imaginary. Data fitting must be adjusted for this.

        The entire domain of the function will be evaluated since some models
        require chunks of data. Then, only the first item will be checked for
        being a complex or real number.

        Returns:
            bool: True if function output is complex else False

        """
        calc = self.f(self.X, self.state) if self.interactive else self.f(self.X)
        return True if "complex" in calc[0].__class__.__name__ else False

    def generate_cds_data(self, x, y):
        """Generate ColumnDataSource data accounting for complex or real functions and logarithmic scale.

        Complex data will not be stored in a CDS since it is not JSON serializable.

        Args:
            x (array): The "x" array should have only real values
            y (array): The "y" array can be complex or real.

        Returns:
            dict: Packaged data for use with a ColumnDataSource

        """
        def maybe_abs(x):
            return np.abs(x) if (x < 0).any() else x

        if self.logx:
            xdata = {"x_log": maybe_abs(x)}
        else:
            xdata = {"x": x}

        if self.complex:
            if self.logy:
                ydata = {"y_real_log": maybe_abs(y.real), "y_imag_log": maybe_abs(y.imag)}
            else:
                ydata = {"y_real": y.real, "y_imag": y.imag}
        else:
            if self.logy:
                ydata = {"y_log": maybe_abs(y)}
            else:
                ydata = {"y": y}

        return {**xdata, **ydata}

    def state_from_input(self, input_params, stepsize=.01):
        """Extract constants and variables from a given parameter dict.

        Aside from creating the state dict that is used as input for the input
        function, this method initializes the sliders for each dynamic variable.
        If the variable value is given by a range, the third item is set as the
        initial value of the slider. If only two values are given, the first is chosen.

        Assuming that you would want to fit the data, the initial value of the
        sliders and their range are used as parameters for the fitting procedure.

        Args:
            params (dict): Parameters for the function.
            stepsize (float, optional): Slider stepsize.

        """
        for k, v in input_params.items():

            if isinstance(v, tuple):

                # Third element is the initial_value if there is one
                initial_value = v[2] if len(v) == 3 else v[0]
                mini = v[0]
                maxi = v[1]

                # Add LMFIT Parameters when there is data
                if hasattr(self, "parameters"):
                    self.parameters.add(name=k, value=initial_value, min=mini, max=maxi)

                self.sliders.append(Slider(
                    start=mini, end=maxi, value=initial_value, step=stepsize, title=k))

            else:
                initial_value = v
                self.parameters.add(name=k, value=initial_value)

            self.state[k] = initial_value

    def callback_slider(self, attr, old, new):
        """Modify model line source data to new slider values.

        Args:
            attr (str): Event name that caused the callback to trigger,
                'value' in this case.
            old (float): Old slider setting.
            new (float): New slider setting

        """
        if new != 0:
            for slider in self.sliders:
                self.state[slider.title] = slider.value

            self.source_model.data = self.generate_cds_data(self.X, self.f(self.X, self.state))

    def modify_doc(self, doc):
        """Define valid function for FunctionHandler.

        All slider interactions and button initiliation is done here.
        The bokeh app can be embedded in notebooks. This method is derived from
        https://github.com/bokeh/bokeh/blob/master/examples/howto/server_embed/notebook_embed.ipynb

        Args:
            doc (Document object): call this function using bokeh.io.show

        """
        for slider in self.sliders:
            slider.on_change('value', self.callback_slider)

        # Configure the layout of the document
        for plot in self.plots:
            # plot.legend.location = optimal_legend_location(
            #     self.source_data if hasattr(self, "source_data") else self.source_model)

            plot.legend.click_policy = "mute"

        doc.add_root(
            column(
                row(*self.buttons),
                row(column(*self.plots), widgetbox(self.sliders)),
                row(self.div)
            )
        )


class DataPlot(Plot):
    """Display both a dataset and a corresponding plot of a model.

    Inherits barebones structure with sliders from Plot. Adds callback methods that
    use lmfit to fit data from selection. All data is selected by default.

    .. todo::
       Display message when performing longer fits

    Args:
        f (function): A function with x as input and named variables in a dict `f(x, p)`.
        params (dict): Labelled parameters initialized by a single number or a range.
        data (dict): Dataset to display along the model f.
        fit_settings (dict, optional): Which fitting method to use.

    """
    def __init__(self, f, params, data, fit_settings={}, **figure_settings):
        """Initialize attributes necessary for fitting and everything else from Plot."""
        self.fit_settings = fit_settings
        if not self.fit_settings.get("method"):
            self.fit_settings["method"] = "leastsq"

        # Integer range used for selecting data subset
        self.data_selection = []

        # This is the data that will be used for fitting since displayed data can morph
        self.data_calc = {k: np.array(v) for k, v in data.items()}

        # Use data range as model limits, account for finding last element in pandas DataFrame
        lim = (
            data["x"][0],
            data["x"].tail(1).index.item() if data["x"].__class__.__name__ == "Series" else data["x"][-1]
        )

        super().__init__(f, params, lim, **figure_settings)

        # Use data_calc since it's already in ndarray form
        self.source_data = ColumnDataSource(
            self.generate_cds_data(self.data_calc["x"], self.data_calc["y"]))

        # Make empty data for the fitted line that will be made visible after the fit is performed
        self.source_fit = ColumnDataSource(self.generate_cds_data(np.array([]), np.array([])))

        # Plots are named after the y label of the data source. Here, data is matched to
        # the right figure.
        if self.complex:
            for plot in self.plots:
                if plot.select(name=self.y_real_key):
                    self.add_data_to_plot(self.y_real_key, plot)
                else:
                    self.add_data_to_plot(self.y_imag_key, plot)
        else:
            self.add_data_to_plot(self.y_key, self.plots[0])

        self.div = Div(text="""Results will be displayed here.""", width=600, height=150)

        button_fit = Button(label="Fit model", button_type="success")
        button_fit.on_click(self.callback_fit)
        self.buttons.append(button_fit)

    def add_data_to_plot(self, y_key, plot):
        """Show data with certain y key on a plot."""
        data_plot = plot.scatter(
            self.x_key, y_key, source=self.source_data, **settings["scatter"])

        plot.add_tools(
            HoverTool(tooltips=[
                ("index", "$index"),
                ("data (x,y)", f"(@{self.x_key}, @{y_key})")  # @ shows actual data and $ shows canvas coords
            ])
        )

        plot.line(self.x_key, y_key, source=self.source_fit, **settings["fit"])

        # Setup errorbar data
        if "yerr" in self.data_calc:
            yerr = self.data_calc["yerr"]
        else:
            yerr = self.data_calc["y"]
            self.data_calc["yerr"] = yerr

        y_lower = self.data_calc["y"] - yerr
        y_upper = self.data_calc["y"] + yerr

        source_error = ColumnDataSource(
            data=dict(
                base=self.data_calc["x"],
                lower=y_lower.imag if "imag" in y_key else y_lower.real,
                upper=y_upper.imag if "imag" in y_key else y_upper.real
            )
        )

        # Resrict selection to horizontal direction only
        plot.add_tools(BoxSelectTool(dimensions="width"))
        plot.select_one(BoxSelectTool).overlay.fill_color = None
        plot.add_layout(Whisker(source=source_error, base="base", upper="upper", lower="lower"))

        # The selected region will always be white, while the ignored region will be colored gray
        left_border = self.X[0]
        right_border = self.X[-1]

        self.selected_region_left = BoxAnnotation(right=left_border, **settings['selection'])
        self.selected_region_right = BoxAnnotation(left=right_border, **settings['selection'])
        plot.add_layout(self.selected_region_left)
        plot.add_layout(self.selected_region_right)

        data_plot.data_source.on_change('selected', self.callback_select)

    def callback_fit(self):
        """Fit model to data using current slider values as initial parameter values.

        A residual function from fitting.residuals is initialized from selected data
        and minimized. Results are formatted as a html table to be displayed in a
        div below the plot.
        """
        self.div.text = "Fitting model..."
        for slider in self.sliders:
            self.parameters[slider.title].value = slider.value

        result = minimize(
            residuals,
            self.parameters,
            args=(self.f, self.data_calc, self.data_selection, self.fit_settings["method"]),
            **self.fit_settings
        )

        # Refresh fit line by re-evaluating function with fitted parameters
        self.source_fit.data = self.generate_cds_data(
            self.X,
            self.f(self.X, result.params.valuesdict()),
        )

        self.div.text = format_results(result)

    def callback_select(self, attr, old, selection):
        """Convert selected data in the plot to a range of ints."""
        self.data_selection = selection["1d"]["indices"]
        self.selected_region_left.right = self.data_calc["x"][min(self.data_selection)]
        self.selected_region_right.left = self.data_calc["x"][max(self.data_selection)]
