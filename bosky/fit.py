"""Collection of functions that falicitate fitting."""
from lmfit.printfuncs import getfloat_attr
import numpy as np


def residuals(parameters, f, data, sel, method="leastsq"):
    """Calculate residuals between a state of the model and the data.

    This function must be minimized during the fitting.

    Args:
        parameters (Parameters): LMFIT Parameters class
        f (function): A function f(x, p) that can evaluate over x where parameters are extracted from a dict
        data (dict): The actual dataset
        sel (list): array of indices to select subset of data

    Returns:
        ndarray: difference between the actual data and model evaluation.

    """
    def select(key):
        return data[key][sel] if sel else data[key]

    def resid(attr=None):
        return np.abs((getattr(y, attr) - getattr(f(x, parameters), attr)) / getattr(yerr, attr))

    x = select("x")
    y = select("y")
    yerr = select("yerr")

    residuals = resid("real") + resid("imag") if data.get("y_imag") else resid("real")

    return residuals if method is "leastsq" else residuals ** 2


def surround(t):
    """Wrap function output with a html tag."""
    def wrapper(func):
        def content(c):
            return "<{0}>{1}</{0}>".format(t, func(c))
        return content
    return wrapper


def format_results(results):
    """Convert fit results to HTML element to be placed in a div.

    Args:
        results (MinimizerResult): Calculated parameters and metrics from minimization.

    Returns:
        str: Structured output of metrics in the left column and best fit parameters
        in the right column

    """
    @surround("tr")
    def gen_row(row_data, tag="td"):
        return "".join(map(lambda x: "<{0}>{1}</{0}>".format(tag, x), row_data))

    metrics = [
        ("datapoints", getfloat_attr(results, "ndata")),
        ("chi2red", getfloat_attr(results, "redchi")),
        ("aic", getfloat_attr(results, "aic")),
        ("bic", getfloat_attr(results, "bic")),
    ]

    ps = results.params

    params = []
    for p in ps:
        format_style = ".2E" if ps[p].value > 1e4 else ".4f"
        params.append(
            (p, "{1:{0}} ± {2:{0}}".format(format_style, ps[p].value, ps[p].stderr)) if ps[p].stderr
            else (p, format(ps[p].value, format_style)))

    return """
    <div style="{0}">
        <table>
            <tr>
                <th>metric</th>
                <th>value</th>
            </tr>
            {1}
        </table>
    </div>
    <div style="{0}">
        <table>
            <tr>
                <th>parameter</th>
                <th>value</th>
            </tr>
            {2}
        </table>
    </div>
    """.format(
        "display: inline-block; vertical-align: top;",
        "".join(map(gen_row, metrics)),
        "".join(map(gen_row, params)),
    )
