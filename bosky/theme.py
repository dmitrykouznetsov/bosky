muted_color = "#b6b6b6"
muted_alpha = 0.5

settings = {
    "figure": {
        "toolbar_location": "above",
        "tools": "pan,wheel_zoom,box_zoom,reset"
    },
    "line": {
        "legend": "model",
        "line_width": 3,
        "muted_color": muted_color,
        "muted_alpha": muted_alpha,
        "line_color": "#3288bd",
        "line_alpha": 0.8,
        "line_dash": "dashed"
    },
    "scatter": {
        "legend": "data",
        "size": 5,
        "muted_color": "black",
        "fill_color": "black",
        "line_color": "black"
    },
    "fit": {
        "legend": "fit",
        "line_width": 3,
        "muted_color": muted_color,
        "muted_alpha": muted_alpha,
        "line_color": "#CD5C5C",
    },
    "selection": {
        "fill_color": "gray",
        "fill_alpha": 0.1
    }
}
