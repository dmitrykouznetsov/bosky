"""Top level functions to generate common plotting environments."""
from bokeh.io import show, output_notebook
from bokeh.resources import INLINE
from .plotting import Plot, DataPlot

__all__ = [
    "plotmodel",
]


output_notebook(INLINE)


def verify_dataset_structure(dset):
    """Check that the input datastructure has valid keys.

    Args:
        plot (function): Utility function that passes a dataset to DataPlot class.

    Raises:
        AssertionError: Specific names for dataset entries are used.
            Current possibilities are "x", "y", "yerr".

    """
    possible_keys = ["x", "y", "yerr"]
    assert all([d in possible_keys for d in dset.keys()]), \
        "Invalid dataset structure, uncpecified keys: {}".format(
        filter(
            lambda x: x not in possible_keys,
            set().difference(dset.keys(), possible_keys)
        )
    )


def plotmodel(model=None, parameters={}, limits=(0, 10), dataset={}, fit={}, **figure_settings):
    """Instantiate and display a Plot object.

    Args:
        model (function): A function of x `f(x)` or `f(x, p)` with
            optional variables `p` as key-value pairs.
        parameters (dict, optional): Labelled parameters initialized by a single value or tuple
        limits (tuple, optional): Plot limits
        dataset (dict, optional): Dataset with optional errors
        figure_settings (kwargs, optional): Additional figure options
        fit (dict, optional): Which fitting method and settings to use. Methods found on
            https://lmfit.github.io/lmfit-py/fitting.html#the-minimize-function and
            options can be found at https://docs.scipy.org/doc/scipy/reference/optimize.html

    Examples:
        >>> plotmodel(
        ...     model=lambda x, p: np.sin(x * p['frequency'] + p['phase']),
        ...     parameters={
        ...         'frequency': (.5, 2.5),
        ...         'phase': 0,
        ...     },
        ...     dataset={
        ...         "x": [1, 2, 3],
        ...         "y": [4, 5, 6]
        ...     }
        ... )

    """
    verify_dataset_structure(dataset)
    if dataset:
        p = DataPlot(model, parameters, dataset, fit, **figure_settings)
    else:
        p = Plot(model, parameters, limits, **figure_settings)

    show(p.modify_doc)