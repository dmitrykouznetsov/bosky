bosky package
=============

bosky.fit module
----------------

.. automodule:: bosky.fit
    :members:
    :undoc-members:
    :show-inheritance:

bosky.main module
-----------------

.. automodule:: bosky.main
    :members:
    :undoc-members:
    :show-inheritance:

bosky.plotting module
---------------------

.. automodule:: bosky.plotting
    :members:
    :undoc-members:
    :show-inheritance:

Module contents
---------------

.. automodule:: bosky
    :members:
    :undoc-members:
    :show-inheritance:
