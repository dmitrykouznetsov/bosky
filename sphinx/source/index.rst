.. bosky documentation master file, created by
   sphinx-quickstart on Mon Feb  5 21:53:59 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Introduction <self>
   API <bosky>
