from setuptools import setup

setup(
    name='bosky',
    version='0.1',
    description='Convenient model exploration.',
    url='https://github.com/dmitrykouznetsov/bosky',
    author='Dmitry Kouznetsov',
    license='MIT',
    packages=['bosky'],

    install_requires=[
        'numpy',
        'bokeh',
        'lmfit',
        'IPython'
    ],

    extras_require={
        'test': [
            'pytest',
            'pytest-cov',
            'pytest-testdox'
        ]
    },

    zip_safe=False
)
