Bosky
=====

.. image:: https://travis-ci.org/dmitrykouznetsov/bosky.svg?branch=master
    :target: https://travis-ci.org/dmitrykouznetsov/bosky

*See the forest for the trees.*

Install
-------

.. code-block:: bash

    pip install git+https://github.com/dmitrykouznetsov/bosky.git

Or alternatively,

.. code-block:: bash

    git clone git@github.com:dmitrykouznetsov/bosky.git
    cd bosky
    make install

Showcase
--------

Examples of model exploration with datasets are found in the `examples <https://github.com/dmitrykouznetsov/bosky/tree/master/examples>`_ folder.
They can be explored with  `jupyterlab <https://github.com/jupyterlab/jupyterlab>`_ or jupyter notebook.
