import pytest


@pytest.fixture
def testcds():
    from bokeh.models import ColumnDataSource
    return ColumnDataSource(dict(
        x=[-1, -1, 0, 1], y=[1, -1, 0, -1]))


@pytest.fixture
def bplt():
    import bosky.plotting as bplt
    return bplt


def test_correct_legend_position(testcds, bplt):
    pos = bplt.optimal_legend_location(testcds)
    assert pos == "top_right", f"""For the plot setup (x are points)
    (-1, 1)    |
    ---------(0, 0)-------
    (-1, -1)---|---(1, -1)
    Legend position should be top_right, but is calculated to be: {pos}"""
