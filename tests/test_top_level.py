import pytest


@pytest.fixture
def sin_input():
    import numpy as np

    x_mock_data = np.linspace(0, 6)
    y_mock_data = list(map(lambda x: np.sin(x) + np.random.normal(0, .1), x_mock_data))

    return dict(
        model=lambda x, p: np.sin(x * p['frequency'] + p['phase']),
        parameters={
            'frequency': (.5, 2.5),
            'phase': 0,
        },
        dataset={
            "x": x_mock_data,
            "y": y_mock_data
        },
    )


@pytest.fixture
def complex_input():
    return dict(
        model=lambda x, p: x * (1 + 2j) * p["shift"],
        parameters={"shift": (0, 4)},
        dataset={"x": [0, 2, 4], "y": [0 + 0j, 1 + 2j, 2 + 4j]},
    )


@pytest.fixture
def bplt():
    from bosky import plotmodel
    return plotmodel


def test_sinewave_plot(bplt, sin_input):
    bplt(**sin_input)
    assert True, "Plotting failed"


def test_complex_function(bplt, complex_input):
    bplt(**complex_input)
    assert True, "Plotting failed"
