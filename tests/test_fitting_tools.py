import pytest


def equal_np_arrays(a, b):
    """Compare all elements in two numpy arrays.

    Args:
        a (ndarray): First array
        b (ndarray): Second array

    Returns:
        bool: True is all elements are equal, False otherwise

    """
    return (a == b).all()


@pytest.fixture
def testresult():
    import numpy as np
    from lmfit import Minimizer, Parameters

    # create data to be fitted
    x = np.linspace(0, 15, 301)
    data = (5. * np.sin(2 * x - 0.1) * np.exp(-x * x * 0.025) +
            np.random.normal(size=len(x), scale=0.2))

    # define objective function: returns the array to be minimized
    def fcn2min(params, x, data):
        """Model a decaying sine wave and subtract data."""
        amp = params['amp']
        shift = params['shift']
        omega = params['omega']
        decay = params['decay']
        model = amp * np.sin(x * omega + shift) * np.exp(-x * x * decay)
        return model - data

    # create a set of Parameters
    params = Parameters()
    params.add('amp', value=10, min=0)
    params.add('decay', value=0.1)
    params.add('shift', value=0.0, min=-np.pi / 2., max=np.pi / 2)
    params.add('omega', value=3.0)

    # do fit, here with leastsq model
    minner = Minimizer(fcn2min, params, fcn_args=(x, data))
    return minner.minimize()


@pytest.fixture
def formatter():
    from bosky.fit import format_results
    return format_results


def test_formatting_results_as_str(testresult, formatter):
    assert type(formatter(testresult)) is str, f"Results from minimization are not formatted as str."
