SRCDIR    = bosky
DOCSDIR   = docs
SPHINXDIR = sphinx

all: install doc test clean

install:
	@python setup.py install --user

doc:
	@rm -r $(DOCSDIR)
	@cd $(SPHINXDIR) ; make html
	@cp -r $(SPHINXDIR)/build/html/ $(DOCSDIR)/

test:
	@python -m pytest \
		--testdox \
		--cov=$(SRCDIR)

clean:
	@rm -rf *.egg-info
